CREATE DATABASE tweb;
USE tweb;

CREATE TABLE users
(
    id          INT          NOT NULL AUTO_INCREMENT,
    username    VARCHAR(50)  NOT NULL,
    email       VARCHAR(250) NOT NULL,
    password    CHAR(72)     NOT NULL,
    is_admin    BOOL         NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO `users` VALUES
(1, 'user', 'user@user.user', '$2y$10$6WfUBO3qbmppTAMpObt07O90auJbJ8iCEfvaAVAsZClpJpJIhQRQO', 0),
(2, 'user2', 'user2@user.user', '$2y$10$v7MZBFiGCoiX9tgIzRxQCuwH1fqycxFkNq8zaT0oPIYl33h/cVzki', 0);

CREATE TABLE cities
(
    id     INT          NOT NULL AUTO_INCREMENT,
    city   VARCHAR(250) NOT NULL,
    region VARCHAR(250) NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO cities
VALUES (NULL, "Aosta", "Valle d'Aosta");
INSERT INTO cities
VALUES (NULL, "Sarre", "Valle d'Aosta");
INSERT INTO cities
VALUES (NULL, "Châtillon", "Valle d'Aosta");
INSERT INTO cities
VALUES (NULL, "Quart", "Valle d'Aosta");
INSERT INTO cities
VALUES (NULL, "Pont-Saint-Martin", "Valle d'Aosta");
INSERT INTO cities
VALUES (NULL, "Saint-Christophe", "Valle d'Aosta");
INSERT INTO cities
VALUES (NULL, "Gressan", "Valle d'Aosta");
INSERT INTO cities
VALUES (NULL, "Saint-Pierre", "Valle d'Aosta");
INSERT INTO cities
VALUES (NULL, "Nus", "Valle d'Aosta");
INSERT INTO cities
VALUES (NULL, "Courmayeur", "Valle d'Aosta");
INSERT INTO cities
VALUES (NULL, "Verrès", "Valle d'Aosta");
INSERT INTO cities
VALUES (NULL, "Donnas", "Valle d'Aosta");
INSERT INTO cities
VALUES (NULL, "Charvensod", "Valle d'Aosta");
INSERT INTO cities
VALUES (NULL, "Valtournenche", "Valle d'Aosta");
INSERT INTO cities
VALUES (NULL, "Morgex", "Valle d'Aosta");
INSERT INTO cities
VALUES (NULL, "Aymavilles", "Valle d'Aosta");
INSERT INTO cities
VALUES (NULL, "La Salle", "Valle d'Aosta");
INSERT INTO cities
VALUES (NULL, "Fénis", "Valle d'Aosta");
INSERT INTO cities
VALUES (NULL, "Montjovet", "Valle d'Aosta");
INSERT INTO cities
VALUES (NULL, "Gignod", "Valle d'Aosta");
INSERT INTO cities
VALUES (NULL, "Pollein", "Valle d'Aosta");
INSERT INTO cities
VALUES (NULL, "Cogne", "Valle d'Aosta");
INSERT INTO cities
VALUES (NULL, "Ayas", "Valle d'Aosta");
INSERT INTO cities
VALUES (NULL, "Issogne", "Valle d'Aosta");

INSERT INTO cities
VALUES (NULL, "Torino", "Piemonte");
INSERT INTO cities
VALUES (NULL, "Novara", "Piemonte");
INSERT INTO cities
VALUES (NULL, "Alessandria", "Piemonte");
INSERT INTO cities
VALUES (NULL, "Asti", "Piemonte");
INSERT INTO cities
VALUES (NULL, "Moncalieri", "Piemonte");
INSERT INTO cities
VALUES (NULL, "Cuneo", "Piemonte");
INSERT INTO cities
VALUES (NULL, "Collegno", "Piemonte");
INSERT INTO cities
VALUES (NULL, "Rivoli", "Piemonte");
INSERT INTO cities
VALUES (NULL, "Nichelino", "Piemonte");
INSERT INTO cities
VALUES (NULL, "Settimo Torinese", "Piemonte");
INSERT INTO cities
VALUES (NULL, "Vercelli", "Piemonte");
INSERT INTO cities
VALUES (NULL, "Biella", "Piemonte");
INSERT INTO cities
VALUES (NULL, "Grugliasco", "Piemonte");
INSERT INTO cities
VALUES (NULL, "Chieri", "Piemonte");
INSERT INTO cities
VALUES (NULL, "Pinerolo", "Piemonte");
INSERT INTO cities
VALUES (NULL, "Casale Monferrato", "Piemonte");
INSERT INTO cities
VALUES (NULL, "Venaria Reale", "Piemonte");
INSERT INTO cities
VALUES (NULL, "Alba", "Piemonte");
INSERT INTO cities
VALUES (NULL, "Verbania", "Piemonte");
INSERT INTO cities
VALUES (NULL, "Bra", "Piemonte");
INSERT INTO cities
VALUES (NULL, "Carmagnola", "Piemonte");
INSERT INTO cities
VALUES (NULL, "Novi Ligure", "Piemonte");
INSERT INTO cities
VALUES (NULL, "Tortona", "Piemonte");
INSERT INTO cities
VALUES (NULL, "Chivasso", "Piemonte");
INSERT INTO cities
VALUES (NULL, "Fossano", "Piemonte");

INSERT INTO cities
VALUES (NULL, "Trento", "Trentino-Alto Adige");
INSERT INTO cities
VALUES (NULL, "Bolzano", "Trentino-Alto Adige");
INSERT INTO cities
VALUES (NULL, "Merano", "Trentino-Alto Adige");
INSERT INTO cities
VALUES (NULL, "Rovereto", "Trentino-Alto Adige");
INSERT INTO cities
VALUES (NULL, "Bressanone", "Trentino-Alto Adige");
INSERT INTO cities
VALUES (NULL, "Pergine Valsugana", "Trentino-Alto Adige");
INSERT INTO cities
VALUES (NULL, "Laives", "Trentino-Alto Adige");
INSERT INTO cities
VALUES (NULL, "Arco", "Trentino-Alto Adige");
INSERT INTO cities
VALUES (NULL, "Riva del Garda", "Trentino-Alto Adige");
INSERT INTO cities
VALUES (NULL, "Brunico", "Trentino-Alto Adige");
INSERT INTO cities
VALUES (NULL, "Appiano sulla Strada del Vino", "Trentino-Alto Adige");
INSERT INTO cities
VALUES (NULL, "Lana", "Trentino-Alto Adige");
INSERT INTO cities
VALUES (NULL, "Mori", "Trentino-Alto Adige");
INSERT INTO cities
VALUES (NULL, "Ala", "Trentino-Alto Adige");
INSERT INTO cities
VALUES (NULL, "Lavis", "Trentino-Alto Adige");
INSERT INTO cities
VALUES (NULL, "Caldaro sulla Strada del Vino", "Trentino-Alto Adige");
INSERT INTO cities
VALUES (NULL, "Levico Terme", "Trentino-Alto Adige");
INSERT INTO cities
VALUES (NULL, "Renon", "Trentino-Alto Adige");
INSERT INTO cities
VALUES (NULL, "Mezzolombardo", "Trentino-Alto Adige");
INSERT INTO cities
VALUES (NULL, "Sarentino", "Trentino-Alto Adige");
INSERT INTO cities
VALUES (NULL, "Cles", "Trentino-Alto Adige");
INSERT INTO cities
VALUES (NULL, "Borgo Valsugana", "Trentino-Alto Adige");
INSERT INTO cities
VALUES (NULL, "Vipiteno", "Trentino-Alto Adige");
INSERT INTO cities
VALUES (NULL, "Castelrotto", "Trentino-Alto Adige");
INSERT INTO cities
VALUES (NULL, "Predaia", "Trentino-Alto Adige");

INSERT INTO cities
VALUES (NULL, "Venezia", "Veneto");
INSERT INTO cities
VALUES (NULL, "Verona", "Veneto");
INSERT INTO cities
VALUES (NULL, "Padova", "Veneto");
INSERT INTO cities
VALUES (NULL, "Vicenza", "Veneto");
INSERT INTO cities
VALUES (NULL, "Treviso", "Veneto");
INSERT INTO cities
VALUES (NULL, "Rovigo", "Veneto");
INSERT INTO cities
VALUES (NULL, "Chioggia", "Veneto");
INSERT INTO cities
VALUES (NULL, "Bassano del Grappa", "Veneto");
INSERT INTO cities
VALUES (NULL, "San Donà di Piave", "Veneto");
INSERT INTO cities
VALUES (NULL, "Schio", "Veneto");
INSERT INTO cities
VALUES (NULL, "Mira", "Veneto");
INSERT INTO cities
VALUES (NULL, "Belluno", "Veneto");
INSERT INTO cities
VALUES (NULL, "Conegliano", "Veneto");
INSERT INTO cities
VALUES (NULL, "Castelfranco Veneto", "Veneto");
INSERT INTO cities
VALUES (NULL, "Villafranca di Verona", "Veneto");
INSERT INTO cities
VALUES (NULL, "Montebelluna", "Veneto");
INSERT INTO cities
VALUES (NULL, "Vittorio Veneto", "Veneto");
INSERT INTO cities
VALUES (NULL, "Mogliano Veneto", "Veneto");
INSERT INTO cities
VALUES (NULL, "Spinea", "Veneto");
INSERT INTO cities
VALUES (NULL, "Mirano", "Veneto");
INSERT INTO cities
VALUES (NULL, "Jesolo", "Veneto");
INSERT INTO cities
VALUES (NULL, "Valdagno", "Veneto");
INSERT INTO cities
VALUES (NULL, "Albignasego", "Veneto");
INSERT INTO cities
VALUES (NULL, "Arzignano", "Veneto");
INSERT INTO cities
VALUES (NULL, "Legnago", "Veneto");

INSERT INTO cities
VALUES (NULL, "Trieste", "Friuli-Venezia Giulia");
INSERT INTO cities
VALUES (NULL, "Udine", "Friuli-Venezia Giulia");
INSERT INTO cities
VALUES (NULL, "Pordenone", "Friuli-Venezia Giulia");
INSERT INTO cities
VALUES (NULL, "Gorizia", "Friuli-Venezia Giulia");
INSERT INTO cities
VALUES (NULL, "Monfalcone", "Friuli-Venezia Giulia");
INSERT INTO cities
VALUES (NULL, "Sacile", "Friuli-Venezia Giulia");
INSERT INTO cities
VALUES (NULL, "Cordenons", "Friuli-Venezia Giulia");
INSERT INTO cities
VALUES (NULL, "Codroipo", "Friuli-Venezia Giulia");
INSERT INTO cities
VALUES (NULL, "Azzano Decimo", "Friuli-Venezia Giulia");
INSERT INTO cities
VALUES (NULL, "Porcia", "Friuli-Venezia Giulia");
INSERT INTO cities
VALUES (NULL, "San Vito al Tagliamento", "Friuli-Venezia Giulia");
INSERT INTO cities
VALUES (NULL, "Tavagnacco", "Friuli-Venezia Giulia");
INSERT INTO cities
VALUES (NULL, "Cervignano del Friuli", "Friuli-Venezia Giulia");
INSERT INTO cities
VALUES (NULL, "Latisana", "Friuli-Venezia Giulia");
INSERT INTO cities
VALUES (NULL, "Muggia", "Friuli-Venezia Giulia");
INSERT INTO cities
VALUES (NULL, "Fontanafredda", "Friuli-Venezia Giulia");
INSERT INTO cities
VALUES (NULL, "Spilimbergo", "Friuli-Venezia Giulia");
INSERT INTO cities
VALUES (NULL, "Ronchi dei Legionari", "Friuli-Venezia Giulia");
INSERT INTO cities
VALUES (NULL, "Maniago", "Friuli-Venezia Giulia");
INSERT INTO cities
VALUES (NULL, "Fiume Veneto", "Friuli-Venezia Giulia");
INSERT INTO cities
VALUES (NULL, "Cividale del Friuli", "Friuli-Venezia Giulia");
INSERT INTO cities
VALUES (NULL, "Gemona del Friuli", "Friuli-Venezia Giulia");
INSERT INTO cities
VALUES (NULL, "Tolmezzo", "Friuli-Venezia Giulia");
INSERT INTO cities
VALUES (NULL, "Pasian di Prato", "Friuli-Venezia Giulia");
INSERT INTO cities
VALUES (NULL, "Brugnera", "Friuli-Venezia Giulia");

INSERT INTO cities
VALUES (NULL, "Milano", "Lombardia");
INSERT INTO cities
VALUES (NULL, "Brescia", "Lombardia");
INSERT INTO cities
VALUES (NULL, "Monza", "Lombardia");
INSERT INTO cities
VALUES (NULL, "Bergamo", "Lombardia");
INSERT INTO cities
VALUES (NULL, "Como", "Lombardia");
INSERT INTO cities
VALUES (NULL, "Busto Arsizio", "Lombardia");
INSERT INTO cities
VALUES (NULL, "Sesto San Giovanni", "Lombardia");
INSERT INTO cities
VALUES (NULL, "Varese", "Lombardia");
INSERT INTO cities
VALUES (NULL, "Cinisello Balsamo", "Lombardia");
INSERT INTO cities
VALUES (NULL, "Pavia", "Lombardia");
INSERT INTO cities
VALUES (NULL, "Cremona", "Lombardia");
INSERT INTO cities
VALUES (NULL, "Vigevano", "Lombardia");
INSERT INTO cities
VALUES (NULL, "Legnano", "Lombardia");
INSERT INTO cities
VALUES (NULL, "Gallarate", "Lombardia");
INSERT INTO cities
VALUES (NULL, "Rho", "Lombardia");
INSERT INTO cities
VALUES (NULL, "Mantova", "Lombardia");
INSERT INTO cities
VALUES (NULL, "Lecco", "Lombardia");
INSERT INTO cities
VALUES (NULL, "Cologno Monzese", "Lombardia");
INSERT INTO cities
VALUES (NULL, "Paderno Dugnano", "Lombardia");
INSERT INTO cities
VALUES (NULL, "Lissone", "Lombardia");
INSERT INTO cities
VALUES (NULL, "Lodi", "Lombardia");
INSERT INTO cities
VALUES (NULL, "Seregno", "Lombardia");
INSERT INTO cities
VALUES (NULL, "Rozzano", "Lombardia");
INSERT INTO cities
VALUES (NULL, "Desio", "Lombardia");
INSERT INTO cities
VALUES (NULL, "Cantù", "Lombardia");

INSERT INTO cities
VALUES (NULL, "Genova", "Liguria");
INSERT INTO cities
VALUES (NULL, "La Spezia", "Liguria");
INSERT INTO cities
VALUES (NULL, "Savona", "Liguria");
INSERT INTO cities
VALUES (NULL, "Sanremo", "Liguria");
INSERT INTO cities
VALUES (NULL, "Imperia", "Liguria");
INSERT INTO cities
VALUES (NULL, "Rapallo", "Liguria");
INSERT INTO cities
VALUES (NULL, "Chiavari", "Liguria");
INSERT INTO cities
VALUES (NULL, "Ventimiglia", "Liguria");
INSERT INTO cities
VALUES (NULL, "Albenga", "Liguria");
INSERT INTO cities
VALUES (NULL, "Sarzana", "Liguria");
INSERT INTO cities
VALUES (NULL, "Sestri Levante", "Liguria");
INSERT INTO cities
VALUES (NULL, "Taggia", "Liguria");
INSERT INTO cities
VALUES (NULL, "Cairo Montenotte", "Liguria");
INSERT INTO cities
VALUES (NULL, "Varazze", "Liguria");
INSERT INTO cities
VALUES (NULL, "Lavagna", "Liguria");
INSERT INTO cities
VALUES (NULL, "Finale Ligure", "Liguria");
INSERT INTO cities
VALUES (NULL, "Arenzano", "Liguria");
INSERT INTO cities
VALUES (NULL, "Loano", "Liguria");
INSERT INTO cities
VALUES (NULL, "Alassio", "Liguria");
INSERT INTO cities
VALUES (NULL, "Arcola", "Liguria");
INSERT INTO cities
VALUES (NULL, "Bordighera", "Liguria");
INSERT INTO cities
VALUES (NULL, "Lerici", "Liguria");
INSERT INTO cities
VALUES (NULL, "Albisola Superiore", "Liguria");
INSERT INTO cities
VALUES (NULL, "Recco", "Liguria");
INSERT INTO cities
VALUES (NULL, "Santo Stefano di Magra", "Liguria");

INSERT INTO cities
VALUES (NULL, "Bologna", "Emilia-Romagna");
INSERT INTO cities
VALUES (NULL, "Parma", "Emilia-Romagna");
INSERT INTO cities
VALUES (NULL, "Modena", "Emilia-Romagna");
INSERT INTO cities
VALUES (NULL, "Reggio Emilia", "Emilia-Romagna");
INSERT INTO cities
VALUES (NULL, "Ravenna", "Emilia-Romagna");
INSERT INTO cities
VALUES (NULL, "Rimini", "Emilia-Romagna");
INSERT INTO cities
VALUES (NULL, "Ferrara", "Emilia-Romagna");
INSERT INTO cities
VALUES (NULL, "Forlì", "Emilia-Romagna");
INSERT INTO cities
VALUES (NULL, "Piacenza", "Emilia-Romagna");
INSERT INTO cities
VALUES (NULL, "Cesena", "Emilia-Romagna");
INSERT INTO cities
VALUES (NULL, "Carpi", "Emilia-Romagna");
INSERT INTO cities
VALUES (NULL, "Imola", "Emilia-Romagna");
INSERT INTO cities
VALUES (NULL, "Faenza", "Emilia-Romagna");
INSERT INTO cities
VALUES (NULL, "Sassuolo", "Emilia-Romagna");
INSERT INTO cities
VALUES (NULL, "Casalecchio di Reno", "Emilia-Romagna");
INSERT INTO cities
VALUES (NULL, "Cento", "Emilia-Romagna");
INSERT INTO cities
VALUES (NULL, "Riccione", "Emilia-Romagna");
INSERT INTO cities
VALUES (NULL, "Formigine", "Emilia-Romagna");
INSERT INTO cities
VALUES (NULL, "Castelfranco Emilia", "Emilia-Romagna");
INSERT INTO cities
VALUES (NULL, "Lugo", "Emilia-Romagna");
INSERT INTO cities
VALUES (NULL, "San Lazzaro di Savena", "Emilia-Romagna");
INSERT INTO cities
VALUES (NULL, "Valsamoggia", "Emilia-Romagna");
INSERT INTO cities
VALUES (NULL, "Cervia", "Emilia-Romagna");
INSERT INTO cities
VALUES (NULL, "San Giovanni in Persiceto", "Emilia-Romagna");
INSERT INTO cities
VALUES (NULL, "Fidenza", "Emilia-Romagna");

INSERT INTO cities
VALUES (NULL, "Firenze", "Toscana");
INSERT INTO cities
VALUES (NULL, "Prato", "Toscana");
INSERT INTO cities
VALUES (NULL, "Livorno", "Toscana");
INSERT INTO cities
VALUES (NULL, "Arezzo", "Toscana");
INSERT INTO cities
VALUES (NULL, "Pisa", "Toscana");
INSERT INTO cities
VALUES (NULL, "Pistoia", "Toscana");
INSERT INTO cities
VALUES (NULL, "Lucca", "Toscana");
INSERT INTO cities
VALUES (NULL, "Grosseto", "Toscana");
INSERT INTO cities
VALUES (NULL, "Massa", "Toscana");
INSERT INTO cities
VALUES (NULL, "Carrara", "Toscana");
INSERT INTO cities
VALUES (NULL, "Viareggio", "Toscana");
INSERT INTO cities
VALUES (NULL, "Siena", "Toscana");
INSERT INTO cities
VALUES (NULL, "Scandicci", "Toscana");
INSERT INTO cities
VALUES (NULL, "Sesto Fiorentino", "Toscana");
INSERT INTO cities
VALUES (NULL, "Empoli", "Toscana");
INSERT INTO cities
VALUES (NULL, "Campi Bisenzio", "Toscana");
INSERT INTO cities
VALUES (NULL, "Capannori", "Toscana");
INSERT INTO cities
VALUES (NULL, "Cascina", "Toscana");
INSERT INTO cities
VALUES (NULL, "Piombino", "Toscana");
INSERT INTO cities
VALUES (NULL, "Camaiore", "Toscana");
INSERT INTO cities
VALUES (NULL, "San Giuliano Terme", "Toscana");
INSERT INTO cities
VALUES (NULL, "Rosignano Marittimo", "Toscana");
INSERT INTO cities
VALUES (NULL, "Pontedera", "Toscana");
INSERT INTO cities
VALUES (NULL, "Poggibonsi", "Toscana");
INSERT INTO cities
VALUES (NULL, "Cecina", "Toscana");

INSERT INTO cities
VALUES (NULL, "Ancona", "Marche");
INSERT INTO cities
VALUES (NULL, "Pesaro", "Marche");
INSERT INTO cities
VALUES (NULL, "Fano", "Marche");
INSERT INTO cities
VALUES (NULL, "Ascoli Piceno", "Marche");
INSERT INTO cities
VALUES (NULL, "San Benedetto del Tronto", "Marche");
INSERT INTO cities
VALUES (NULL, "Senigallia", "Marche");
INSERT INTO cities
VALUES (NULL, "Civitanova Marche", "Marche");
INSERT INTO cities
VALUES (NULL, "Macerata", "Marche");
INSERT INTO cities
VALUES (NULL, "Jesi", "Marche");
INSERT INTO cities
VALUES (NULL, "Fermo", "Marche");
INSERT INTO cities
VALUES (NULL, "Osimo", "Marche");
INSERT INTO cities
VALUES (NULL, "Fabriano", "Marche");
INSERT INTO cities
VALUES (NULL, "Falconara Marittima", "Marche");
INSERT INTO cities
VALUES (NULL, "Porto Sant'Elpidio", "Marche");
INSERT INTO cities
VALUES (NULL, "Recanati", "Marche");
INSERT INTO cities
VALUES (NULL, "Tolentino", "Marche");
INSERT INTO cities
VALUES (NULL, "Castelfidardo", "Marche");
INSERT INTO cities
VALUES (NULL, "Sant'Elpidio a Mare", "Marche");
INSERT INTO cities
VALUES (NULL, "Grottammare", "Marche");
INSERT INTO cities
VALUES (NULL, "Porto San Giorgio", "Marche");
INSERT INTO cities
VALUES (NULL, "Potenza Picena", "Marche");
INSERT INTO cities
VALUES (NULL, "Corridonia", "Marche");
INSERT INTO cities
VALUES (NULL, "Vallefoglia", "Marche");
INSERT INTO cities
VALUES (NULL, "Urbino", "Marche");
INSERT INTO cities
VALUES (NULL, "Chiaravalle", "Marche");

INSERT INTO cities
VALUES (NULL, "Perugia", "Umbria");
INSERT INTO cities
VALUES (NULL, "Terni", "Umbria");
INSERT INTO cities
VALUES (NULL, "Foligno", "Umbria");
INSERT INTO cities
VALUES (NULL, "Città di Castello", "Umbria");
INSERT INTO cities
VALUES (NULL, "Spoleto", "Umbria");
INSERT INTO cities
VALUES (NULL, "Gubbio", "Umbria");
INSERT INTO cities
VALUES (NULL, "Assisi", "Umbria");
INSERT INTO cities
VALUES (NULL, "Bastia Umbra", "Umbria");
INSERT INTO cities
VALUES (NULL, "Corciano", "Umbria");
INSERT INTO cities
VALUES (NULL, "Orvieto", "Umbria");
INSERT INTO cities
VALUES (NULL, "Narni", "Umbria");
INSERT INTO cities
VALUES (NULL, "Marsciano", "Umbria");
INSERT INTO cities
VALUES (NULL, "Todi", "Umbria");
INSERT INTO cities
VALUES (NULL, "Umbertide", "Umbria");
INSERT INTO cities
VALUES (NULL, "Castiglione del Lago", "Umbria");
INSERT INTO cities
VALUES (NULL, "Gualdo Tadino", "Umbria");
INSERT INTO cities
VALUES (NULL, "Magione", "Umbria");
INSERT INTO cities
VALUES (NULL, "Amelia", "Umbria");
INSERT INTO cities
VALUES (NULL, "San Giustino", "Umbria");
INSERT INTO cities
VALUES (NULL, "Deruta", "Umbria");
INSERT INTO cities
VALUES (NULL, "Spello", "Umbria");
INSERT INTO cities
VALUES (NULL, "Trevi", "Umbria");
INSERT INTO cities
VALUES (NULL, "Città della Pieve", "Umbria");
INSERT INTO cities
VALUES (NULL, "Torgiano", "Umbria");
INSERT INTO cities
VALUES (NULL, "Gualdo Cattaneo", "Umbria");

INSERT INTO cities
VALUES (NULL, "Pescara", "Abruzzo");
INSERT INTO cities
VALUES (NULL, "L'Aquila", "Abruzzo");
INSERT INTO cities
VALUES (NULL, "Teramo", "Abruzzo");
INSERT INTO cities
VALUES (NULL, "Montesilvano", "Abruzzo");
INSERT INTO cities
VALUES (NULL, "Chieti", "Abruzzo");
INSERT INTO cities
VALUES (NULL, "Avezzano", "Abruzzo");
INSERT INTO cities
VALUES (NULL, "Vasto", "Abruzzo");
INSERT INTO cities
VALUES (NULL, "Lanciano", "Abruzzo");
INSERT INTO cities
VALUES (NULL, "Roseto degli Abruzzi", "Abruzzo");
INSERT INTO cities
VALUES (NULL, "Francavilla al Mare", "Abruzzo");
INSERT INTO cities
VALUES (NULL, "Sulmona", "Abruzzo");
INSERT INTO cities
VALUES (NULL, "Giulianova", "Abruzzo");
INSERT INTO cities
VALUES (NULL, "Ortona", "Abruzzo");
INSERT INTO cities
VALUES (NULL, "San Salvo", "Abruzzo");
INSERT INTO cities
VALUES (NULL, "Spoltore", "Abruzzo");
INSERT INTO cities
VALUES (NULL, "Martinsicuro", "Abruzzo");
INSERT INTO cities
VALUES (NULL, "Silvi", "Abruzzo");
INSERT INTO cities
VALUES (NULL, "Città Sant'Angelo", "Abruzzo");
INSERT INTO cities
VALUES (NULL, "Pineto", "Abruzzo");
INSERT INTO cities
VALUES (NULL, "San Giovanni Teatino", "Abruzzo");
INSERT INTO cities
VALUES (NULL, "Alba Adriatica", "Abruzzo");
INSERT INTO cities
VALUES (NULL, "Penne", "Abruzzo");
INSERT INTO cities
VALUES (NULL, "Tortoreto", "Abruzzo");
INSERT INTO cities
VALUES (NULL, "Celano", "Abruzzo");
INSERT INTO cities
VALUES (NULL, "Cepagatti", "Abruzzo");

INSERT INTO cities
VALUES (NULL, "Potenza", "Basilicata");
INSERT INTO cities
VALUES (NULL, "Matera", "Basilicata");
INSERT INTO cities
VALUES (NULL, "Melfi", "Basilicata");
INSERT INTO cities
VALUES (NULL, "Pisticci", "Basilicata");
INSERT INTO cities
VALUES (NULL, "Policoro", "Basilicata");
INSERT INTO cities
VALUES (NULL, "Lavello", "Basilicata");
INSERT INTO cities
VALUES (NULL, "Rionero in Vulture", "Basilicata");
INSERT INTO cities
VALUES (NULL, "Lauria", "Basilicata");
INSERT INTO cities
VALUES (NULL, "Bernalda", "Basilicata");
INSERT INTO cities
VALUES (NULL, "Venosa", "Basilicata");
INSERT INTO cities
VALUES (NULL, "Avigliano", "Basilicata");
INSERT INTO cities
VALUES (NULL, "Montescaglioso", "Basilicata");
INSERT INTO cities
VALUES (NULL, "Ferrandina", "Basilicata");
INSERT INTO cities
VALUES (NULL, "Scanzano Jonico", "Basilicata");
INSERT INTO cities
VALUES (NULL, "Tito", "Basilicata");
INSERT INTO cities
VALUES (NULL, "Montalbano Jonico", "Basilicata");
INSERT INTO cities
VALUES (NULL, "Senise", "Basilicata");
INSERT INTO cities
VALUES (NULL, "Pignola", "Basilicata");
INSERT INTO cities
VALUES (NULL, "Nova Siri", "Basilicata");
INSERT INTO cities
VALUES (NULL, "Sant'Arcangelo", "Basilicata");
INSERT INTO cities
VALUES (NULL, "Picerno", "Basilicata");
INSERT INTO cities
VALUES (NULL, "Genzano di Lucania", "Basilicata");
INSERT INTO cities
VALUES (NULL, "Marsicovetere", "Basilicata");
INSERT INTO cities
VALUES (NULL, "Lagonegro", "Basilicata");
INSERT INTO cities
VALUES (NULL, "Muro Lucano", "Basilicata");

INSERT INTO cities
VALUES (NULL, "Reggio Calabria", "Calabria");
INSERT INTO cities
VALUES (NULL, "Catanzaro", "Calabria");
INSERT INTO cities
VALUES (NULL, "Lamezia Terme", "Calabria");
INSERT INTO cities
VALUES (NULL, "Cosenza", "Calabria");
INSERT INTO cities
VALUES (NULL, "Crotone", "Calabria");
INSERT INTO cities
VALUES (NULL, "Corigliano Calabro", "Calabria");
INSERT INTO cities
VALUES (NULL, "Rossano", "Calabria");
INSERT INTO cities
VALUES (NULL, "Rende", "Calabria");
INSERT INTO cities
VALUES (NULL, "Vibo Valentia", "Calabria");
INSERT INTO cities
VALUES (NULL, "Castrovillari", "Calabria");
INSERT INTO cities
VALUES (NULL, "Acri", "Calabria");
INSERT INTO cities
VALUES (NULL, "Gioia Tauro", "Calabria");
INSERT INTO cities
VALUES (NULL, "Montalto Uffugo", "Calabria");
INSERT INTO cities
VALUES (NULL, "Palmi", "Calabria");
INSERT INTO cities
VALUES (NULL, "Cassano all'Ionio", "Calabria");
INSERT INTO cities
VALUES (NULL, "Siderno", "Calabria");
INSERT INTO cities
VALUES (NULL, "Isola di Capo Rizzuto", "Calabria");
INSERT INTO cities
VALUES (NULL, "San Giovanni in Fiore", "Calabria");
INSERT INTO cities
VALUES (NULL, "Paola", "Calabria");
INSERT INTO cities
VALUES (NULL, "Taurianova", "Calabria");
INSERT INTO cities
VALUES (NULL, "Cirò Marina", "Calabria");
INSERT INTO cities
VALUES (NULL, "Rosarno", "Calabria");
INSERT INTO cities
VALUES (NULL, "Amantea", "Calabria");
INSERT INTO cities
VALUES (NULL, "Villa San Giovanni", "Calabria");
INSERT INTO cities
VALUES (NULL, "Locri", "Calabria");

INSERT INTO cities
VALUES (NULL, "Napoli", "Campania");
INSERT INTO cities
VALUES (NULL, "Salerno", "Campania");
INSERT INTO cities
VALUES (NULL, "Giugliano in Campania", "Campania");
INSERT INTO cities
VALUES (NULL, "Torre del Greco", "Campania");
INSERT INTO cities
VALUES (NULL, "Pozzuoli", "Campania");
INSERT INTO cities
VALUES (NULL, "Casoria", "Campania");
INSERT INTO cities
VALUES (NULL, "Caserta", "Campania");
INSERT INTO cities
VALUES (NULL, "Castellammare di Stabia", "Campania");
INSERT INTO cities
VALUES (NULL, "Afragola", "Campania");
INSERT INTO cities
VALUES (NULL, "Benevento", "Campania");
INSERT INTO cities
VALUES (NULL, "Marano di Napoli", "Campania");
INSERT INTO cities
VALUES (NULL, "Acerra", "Campania");
INSERT INTO cities
VALUES (NULL, "Portici", "Campania");
INSERT INTO cities
VALUES (NULL, "Avellino", "Campania");
INSERT INTO cities
VALUES (NULL, "Cava de' Tirreni", "Campania");
INSERT INTO cities
VALUES (NULL, "Ercolano", "Campania");
INSERT INTO cities
VALUES (NULL, "Aversa", "Campania");
INSERT INTO cities
VALUES (NULL, "Battipaglia", "Campania");
INSERT INTO cities
VALUES (NULL, "Scafati", "Campania");
INSERT INTO cities
VALUES (NULL, "Casalnuovo di Napoli", "Campania");
INSERT INTO cities
VALUES (NULL, "Nocera Inferiore", "Campania");
INSERT INTO cities
VALUES (NULL, "San Giorgio a Cremano", "Campania");
INSERT INTO cities
VALUES (NULL, "Torre Annunziata", "Campania");
INSERT INTO cities
VALUES (NULL, "Quarto", "Campania");
INSERT INTO cities
VALUES (NULL, "Eboli", "Campania");

INSERT INTO cities
VALUES (NULL, "Campobasso", "Molise");
INSERT INTO cities
VALUES (NULL, "Termoli", "Molise");
INSERT INTO cities
VALUES (NULL, "Isernia", "Molise");
INSERT INTO cities
VALUES (NULL, "Venafro", "Molise");
INSERT INTO cities
VALUES (NULL, "Bojano", "Molise");
INSERT INTO cities
VALUES (NULL, "Campomarino", "Molise");
INSERT INTO cities
VALUES (NULL, "Larino", "Molise");
INSERT INTO cities
VALUES (NULL, "Montenero di Bisaccia", "Molise");
INSERT INTO cities
VALUES (NULL, "Guglionesi", "Molise");
INSERT INTO cities
VALUES (NULL, "Riccia", "Molise");
INSERT INTO cities
VALUES (NULL, "Agnone", "Molise");
INSERT INTO cities
VALUES (NULL, "San Martino in Pensilis", "Molise");
INSERT INTO cities
VALUES (NULL, "Trivento", "Molise");
INSERT INTO cities
VALUES (NULL, "Santa Croce di Magliano", "Molise");
INSERT INTO cities
VALUES (NULL, "Petacciato", "Molise");
INSERT INTO cities
VALUES (NULL, "Cercemaggiore", "Molise");
INSERT INTO cities
VALUES (NULL, "Vinchiaturo", "Molise");
INSERT INTO cities
VALUES (NULL, "Ferrazzano", "Molise");
INSERT INTO cities
VALUES (NULL, "Frosolone", "Molise");
INSERT INTO cities
VALUES (NULL, "Ripalimosani", "Molise");
INSERT INTO cities
VALUES (NULL, "Ururi", "Molise");
INSERT INTO cities
VALUES (NULL, "Baranello", "Molise");
INSERT INTO cities
VALUES (NULL, "Portocannone", "Molise");
INSERT INTO cities
VALUES (NULL, "Campodipietra", "Molise");
INSERT INTO cities
VALUES (NULL, "Montaquila", "Molise");

INSERT INTO cities
VALUES (NULL, "Bari", "Puglia");
INSERT INTO cities
VALUES (NULL, "Taranto", "Puglia");
INSERT INTO cities
VALUES (NULL, "Foggia", "Puglia");
INSERT INTO cities
VALUES (NULL, "Andria", "Puglia");
INSERT INTO cities
VALUES (NULL, "Lecce", "Puglia");
INSERT INTO cities
VALUES (NULL, "Barletta", "Puglia");
INSERT INTO cities
VALUES (NULL, "Brindisi", "Puglia");
INSERT INTO cities
VALUES (NULL, "Altamura", "Puglia");
INSERT INTO cities
VALUES (NULL, "Molfetta", "Puglia");
INSERT INTO cities
VALUES (NULL, "Cerignola", "Puglia");
INSERT INTO cities
VALUES (NULL, "Manfredonia", "Puglia");
INSERT INTO cities
VALUES (NULL, "Trani", "Puglia");
INSERT INTO cities
VALUES (NULL, "Bisceglie", "Puglia");
INSERT INTO cities
VALUES (NULL, "Bitonto", "Puglia");
INSERT INTO cities
VALUES (NULL, "San Severo", "Puglia");
INSERT INTO cities
VALUES (NULL, "Monopoli", "Puglia");
INSERT INTO cities
VALUES (NULL, "Martina Franca", "Puglia");
INSERT INTO cities
VALUES (NULL, "Corato", "Puglia");
INSERT INTO cities
VALUES (NULL, "Gravina in Puglia", "Puglia");
INSERT INTO cities
VALUES (NULL, "Fasano", "Puglia");
INSERT INTO cities
VALUES (NULL, "Modugno", "Puglia");
INSERT INTO cities
VALUES (NULL, "Francavilla Fontana", "Puglia");
INSERT INTO cities
VALUES (NULL, "Lucera", "Puglia");
INSERT INTO cities
VALUES (NULL, "Massafra", "Puglia");
INSERT INTO cities
VALUES (NULL, "Grottaglie", "Puglia");

INSERT INTO cities
VALUES (NULL, "Palermo", "Sicilia");
INSERT INTO cities
VALUES (NULL, "Catania", "Sicilia");
INSERT INTO cities
VALUES (NULL, "Messina", "Sicilia");
INSERT INTO cities
VALUES (NULL, "Siracusa", "Sicilia");
INSERT INTO cities
VALUES (NULL, "Marsala", "Sicilia");
INSERT INTO cities
VALUES (NULL, "Gela", "Sicilia");
INSERT INTO cities
VALUES (NULL, "Ragusa", "Sicilia");
INSERT INTO cities
VALUES (NULL, "Trapani", "Sicilia");
INSERT INTO cities
VALUES (NULL, "Vittoria", "Sicilia");
INSERT INTO cities
VALUES (NULL, "Caltanissetta", "Sicilia");
INSERT INTO cities
VALUES (NULL, "Agrigento", "Sicilia");
INSERT INTO cities
VALUES (NULL, "Bagheria", "Sicilia");
INSERT INTO cities
VALUES (NULL, "Modica", "Sicilia");
INSERT INTO cities
VALUES (NULL, "Acireale", "Sicilia");
INSERT INTO cities
VALUES (NULL, "Mazara del Vallo", "Sicilia");
INSERT INTO cities
VALUES (NULL, "Misterbianco", "Sicilia");
INSERT INTO cities
VALUES (NULL, "Paternò", "Sicilia");
INSERT INTO cities
VALUES (NULL, "Alcamo", "Sicilia");
INSERT INTO cities
VALUES (NULL, "Barcellona Pozzo di Gotto", "Sicilia");
INSERT INTO cities
VALUES (NULL, "Sciacca", "Sicilia");
INSERT INTO cities
VALUES (NULL, "Monreale", "Sicilia");
INSERT INTO cities
VALUES (NULL, "Carini	", "Sicilia");
INSERT INTO cities
VALUES (NULL, "Caltagirone", "Sicilia");
INSERT INTO cities
VALUES (NULL, "Licata", "Sicilia");
INSERT INTO cities
VALUES (NULL, "Augusta", "Sicilia");

INSERT INTO cities
VALUES (NULL, "Cagliari", "Sardegna");
INSERT INTO cities
VALUES (NULL, "Sassari", "Sardegna");
INSERT INTO cities
VALUES (NULL, "Quartu Sant'Elena", "Sardegna");
INSERT INTO cities
VALUES (NULL, "Olbia", "Sardegna");
INSERT INTO cities
VALUES (NULL, "Alghero", "Sardegna");
INSERT INTO cities
VALUES (NULL, "Nuoro", "Sardegna");
INSERT INTO cities
VALUES (NULL, "Oristano", "Sardegna");
INSERT INTO cities
VALUES (NULL, "Selargius", "Sardegna");
INSERT INTO cities
VALUES (NULL, "Carbonia", "Sardegna");
INSERT INTO cities
VALUES (NULL, "Iglesias", "Sardegna");
INSERT INTO cities
VALUES (NULL, "Capoterra", "Sardegna");
INSERT INTO cities
VALUES (NULL, "Porto Torres", "Sardegna");
INSERT INTO cities
VALUES (NULL, "Sestu", "Sardegna");
INSERT INTO cities
VALUES (NULL, "Monserrato", "Sardegna");
INSERT INTO cities
VALUES (NULL, "Sinnai", "Sardegna");
INSERT INTO cities
VALUES (NULL, "Sorso", "Sardegna");
INSERT INTO cities
VALUES (NULL, "Tempio Pausania", "Sardegna");
INSERT INTO cities
VALUES (NULL, "Villacidro", "Sardegna");
INSERT INTO cities
VALUES (NULL, "Arzachena", "Sardegna");
INSERT INTO cities
VALUES (NULL, "Quartucciu", "Sardegna");
INSERT INTO cities
VALUES (NULL, "Guspini", "Sardegna");
INSERT INTO cities
VALUES (NULL, "Siniscola", "Sardegna");
INSERT INTO cities
VALUES (NULL, "Sant'Antioco", "Sardegna");
INSERT INTO cities
VALUES (NULL, "La Maddalena", "Sardegna");

CREATE TABLE ads
(
    id        INT          NOT NULL AUTO_INCREMENT,
    user      INT          NOT NULL,
    title     VARCHAR(250) NOT NULL,
    body      TEXT,
    price     INT          NOT NULL,
    published TIMESTAMP    NOT NULL,
    active    BOOL         NOT NULL,
    city      INT          NOT NULL,
    img1      VARCHAR(40)  NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (user) REFERENCES users (id)
        ON DELETE CASCADE
        ON UPDATE NO ACTION,
    FOREIGN KEY (city) REFERENCES cities (id)
        ON DELETE CASCADE
        ON UPDATE NO ACTION
);

INSERT INTO ads VALUES
(1, 1, 'Cerchi In Lega Alfa Romeo Giulietta 18 159', 'CENTRO GOMME SORRENTINO VI PROPONE:\r\n\r\nCerchi in lega Alfa Romeo Brera/Giulietta 18\r\nReplica Sprint\r\n\r\nCerchi nuovi in scatolo completi di accessori\r\n\r\nCaratteristiche tecniche\r\n8.0x18 5x110 et 41 550,00 Euro\r\n\r\nNB NON MONTA CON IMPIANTO FRENANTE BREMBO\r\n\r\nCerchi nuovi in scatolo completi di accessori\r\nCerchi di ottima qualita\' e fattura\r\n\r\nClicca su MI PIACE nella nostra pagina facebook\r\nCentro Gomme Sorrentino\r\ncomunicaci il nome e RICEVERAI 10 EURO DI SCONTO SUL TUO ACQUISTO\r\n\r\nVendiamo pneumatici e cerchi in lega da 20 anni\r\nsiamo un\'azienda affidabile e conosciuta sul territorio\r\nForniamo pneumatici estivi e invernali con deposito stagionale\r\n\r\nCentro Gomme Sorrentino\r\nVia Atzori 104/110\r\n84014 Nocera Inferiore\r\nTel e Fax 0815177176\r\nMobile 393.6014554 Whatsapp\r\nVenite a Trovarci', 550, '2019-09-13 14:02:58', 1, 1, 'c2d025917e8d7c8e447c10aa50bf6cc9.jpg'),
(3, 2, 'Umidificatore', 'Vendesi Umidificatore\r\n\r\nTel 333910293281', 25, '2019-09-13 14:10:21', 1, 168, '3ee09bfcae05a66aba49539c04185ae6.jpg'),
(4, 1, 'Trattore Lamborghini 230 - 2 RM', 'Trattore Lamborghini 230 - 2 RM\r\nDescrizione: con documenti - pronto per il lavoro\r\n\r\nContatto: Luciano 328 4509055 - informazioni solo per telefono!', 10000, '2019-09-13 14:10:21', 1, 225, 'f9284393e1fe7aee82fcb81aeed39d8c.jpg'),
(5, 2, 'Audi Q5 2.0 TDI 170 CV', 'Audi Q5 2.0 TDI 170 CV quattro\r\nImm 04/08/2011\r\nCv170\r\nKm 163.000\r\nUnico proprietario\r\nUltimi lavori eseguiti a 160.000km\r\ntagliando completo + cinghia distribuzione\r\n\r\nIn vendita non compresi nel prezzo\r\nTreno gomme invernali, baule per tetto .\r\n\r\nAlzacristalli elettrici,Bracciolo,Climatizzatore\r\nControllo automatico clima\r\nCruise Control\r\nPark distance control,Sedile posteriore sdoppiato\r\nSensore di luminosità,Sensore di pioggia\r\nSensori di parcheggio assistito posteriori\r\nSistema di navigazione,\r\nSpecchietti laterali elettrici,\r\nVolante in pelle,Volante multifunzione,\r\nAutoradio,Autoradio digitale,Bluetooth\r\nCD,Computer di bordo,MP3,Sound system\r\nUSB,Vivavoce,Cerchi in lega,Controllo vocale,\r\nSki bag,ABS,Adaptive Cruise Control,\r\nAirbag conducente,Airbag laterali\r\nAirbag passeggero\r\nAirbag posteriore\r\nAirbag testa,Antifurto\r\nChiusura centralizzata telecomandata\r\nFari Xenon,Fendinebbia\r\nIsofix,Luci diurne\r\nLuci diurne LED,Servosterzo\r\n\r\nPrezzo comprensivo di garanzia 12 Mesi\r\nPossibilità di finanziare l\'importo', 14900, '2019-09-13 14:12:34', 1, 361, '496778cd74407eef058e68fd68586833.jpg'),
(6, 1, 'Sedie Chiavarine', 'Vendo 6 sedie di Chiavari.\r\nTutte originali e in buonissime condizioni.\r\nPrezzo ragionevolmente trattabile.\r\nPagamento rigorosamente in contanti e ritiro a mano (non spedisco).\r\nRispondo unicamente a telefonate e messaggi da numeri telefonici italiani.\r\nTEL 3453217895', 900, '2019-09-13 14:12:34', 1, 25, '0b3d422c7229e0028cd8640f9197bff6.jpg'),
(7, 1, 'Sgabelli più poltrona', 'Vendo poltrona più 3 sgabelli\r\nTel: 3393501809', 50, '2019-09-13 14:06:39', 1, 25, 'd1eadaba34c653ecf87dacb171c1d6bd.jpg');

