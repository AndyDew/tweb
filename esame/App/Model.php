<?php


class Model
{
    public function removeById(): bool
    {
        $rm = App::getDB()->prepare('DELETE FROM ' . $this->table . ' WHERE id = ?');
        $rm->bindParam(1, $this->id);
        $rm->execute();
        return $rm->rowCount() === 1;
    }

    public function fetchById(): array
    {
        $fetch = App::getDB()->prepare('SELECT * FROM ' . $this->table . ' WHERE id = ?');
        $fetch->bindParam(1, $this->id);
        $fetch->execute();
        return $fetch->fetch(PDO::FETCH_ASSOC);
    }
}