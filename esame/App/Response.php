<?php


class Response
{
    public $status;
    public $data;

    public function __construct(string $status,object $data)
    {
        $this->status = $status;
        $this->data = $data;
        echo json_encode($this);
    }
}