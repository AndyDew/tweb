<?php


class App
{
    private static $db_link = null;
    public static $app_title = 'Sell.it';
    public static $copyright = '&copy;2019 Sell.it S.r.l.s.';

    public static function getDB(): ?\PDO
    {
        if (self::$db_link === null) {
            self::$db_link = new PDO('mysql:host=localhost:8889;dbname=tweb;','root', 'root');
        }
        return self::$db_link;
    }
}