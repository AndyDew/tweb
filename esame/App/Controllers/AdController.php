<?php

class AdController extends Controller
{
    public function index()
    {
        header('Location: /login');
        die();
    }

    public function show(int $id): void
    {
        try {
            $ad = new Ad($id);
        } catch (TypeError $error) {
            header('Location: /');
            die();
        }
        $view_data = new stdClass();

        $view_data->ad = $ad;

        $owner = new User($ad->user);
        $view_data->owner = $owner;


        $this->viewWithData('App/Views/Ad.php', $view_data);
    }

    public function rm($id): void
    {
        $rp_data = new stdClass();
        try {
            $ad = new Ad($id);
        } catch (TypeError $error) {
            $rp_data->msg = 'Inserzione non trovata';
            new Response('FAIL', $rp_data);
            die();
        }

        if ($ad->user === $_SESSION['id'] || $_SESSION['is_admin']) {
            $ad->removeById();
            $rp_data->msg = 'Inserzione rimossa';
            new Response('OK', $rp_data);
        } else {
            $rp_data->msg = 'Azione non permessa';
            new Response('Fail', $rp_data);
        }
    }
}