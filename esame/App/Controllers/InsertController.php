<?php


class InsertController extends Controller
{
    public function index()
    {
        if (!isset($_SESSION['id'])) {
            header('Location: /login');
            die();
        }
        $cities = new City('',0);

        $view_data = new stdClass();
        $view_data->cities = $cities->getCities();

        $this->viewWithData('App/Views/Insert.php', $view_data);
    }


    public function add(): void
    {
        $view_data = new stdClass();

        if (isset($_POST['title'],$_POST['price'],$_POST['city'],$_POST['body'])) {
            $check = getimagesize($_FILES['picture']['tmp_name']);
            if($check !== false) {
                $file_name = md5_file($_FILES['picture']['tmp_name']).'.'.explode('/',$_FILES['picture']['type'])[1];
                $upload = move_uploaded_file($_FILES['picture']['tmp_name'],'uploads/'.$file_name);
                if ($upload) {
                    $ad = new Ad(0);
                    $ad->user = $_SESSION['id'];
                    $ad->title = $_POST['title'];
                    $ad->body = $_POST['body'];
                    $ad->price = $_POST['price'];
                    $date = new Datetime('now');
                    $ad->published = $date->format('Y-m-d H:i:s');
                    $ad->active = true;
                    $ad->city = $_POST['city'];
                    $ad->img1 = $file_name;
                    $ad->create();
                    $view_data->uploaded = true;
                }
            } else {
                $view_data->uploaded = false;
            }
        } else {
            $view_data->uploaded = false;
        }

        $this->viewWithData('App/Views/Insert.php', $view_data);
    }
}