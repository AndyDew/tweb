<?php


class SearchController extends Controller
{
    public function index()
    {
        if (!isset($_SESSION['id'], $_POST['search_query'], $_POST['regionSelector'], $_POST['citySelector'])) {
            header('Location: /login');
            die();
        }

        $ads = new Ad(0);
        $ads = $ads->searchAds($_POST['search_query'], $_POST['regionSelector'], $_POST['citySelector']);

        $view_data = new stdClass();

        $view_data->ads = $ads;
        $view_data->term = htmlspecialchars($_POST['search_query']);

        $this->viewWithData('App/Views/Search.php', $view_data);
    }

}