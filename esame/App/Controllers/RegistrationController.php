<?php

class RegistrationController extends Controller
{
    public function index(): void
    {
        if (isset($_SESSION['id'])) {
            header('Location: /');
            die();
        }
        require 'App/Views/Registration.php';
    }

    public function api(): void
    {
        $json = file_get_contents('php://input');
        $data = json_decode($json,FALSE);
        $rp_data = new stdClass();

        if(isset($data->username,$data->password,$data->email) && filter_var($data->email, FILTER_VALIDATE_EMAIL)) {
            $user = new User(0);
            $user->username = $data->username;
            $user->email = $data->email;
            $user->is_admin = 0;
            $user->new_password = $data->password;
            if($user->create()) {
                $rp_data->msg = 'Registrazione eseguita!';
                new Response('OK',$rp_data);
            } else {
                $rp_data->msg = 'Email o username già in uso!';
                new Response('FAIL',$rp_data);
            }
        } else {
            $rp_data->msg = 'Verifica di aver inserito correttamente i dati!';
            new Response('FAIL',new stdClass());
        }
    }
}