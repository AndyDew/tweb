<?php

class LoginController extends Controller
{
    public function index(): void
    {
        if (isset($_SESSION['id'])) {
            header('Location: /');
            die();
        }
        require 'App/Views/Login.php';
    }

    public function api(): void
    {
        $json = file_get_contents('php://input');
        $data = json_decode($json, FALSE);
        $rp_data = new stdClass();

        if (isset($data->username, $data->password)) {
            $user = new User(0);
            $user->username = $data->username;
            $user->fetchByUsername();

            if (password_verify($data->password, $user->password)) {
                $_SESSION['id'] = $user->id;
                $_SESSION['username'] = $user->username;
                $_SESSION['email'] = $user->email;
                $_SESSION['is_admin'] = $user->is_admin;

                $rp_data->msg = 'Login eseguito!';
                new Response('OK', $rp_data);
            } else {
                $rp_data->msg = 'Account non trovato!';
                new Response('FAIL', $rp_data);
            }
        } else {
            $rp_data->msg = 'Verifica di aver inserito correttamente i dati!';
            new Response('FAIL', $rp_data);
        }

    }

    public function out(): void
    {
        session_destroy();
        header('Location: /');
        die();
    }
}