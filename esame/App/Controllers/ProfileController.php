<?php


class ProfileController extends Controller
{
    public function index()
    {
        if (!isset($_SESSION['id'])) {
            header('Location: /login');
            die();
        }
        $view_data = new stdClass();

        $this->viewWithData('App/Views/Profile.php', $view_data);
    }

    public function passwordChange() {
        $user = new User($_SESSION['id']);
        $json = file_get_contents('php://input');
        $data = json_decode($json, FALSE);
        $rp_data = new stdClass();

        if(password_verify($data->password,$user->password)) {
            $user->new_password = $data->new_password;
            $user->update();
            $rp_data->msg = 'Cambio Password eseguito';
            new Response('OK', $rp_data);
        } else {
            $rp_data->msg = 'Password attuale errata';
            new Response('FAIL', $rp_data);
        }
    }
}