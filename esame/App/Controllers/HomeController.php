<?php

class HomeController extends Controller
{
    public function index()
    {
        if (!isset($_SESSION['id'])) {
            header('Location: /login');
            die();
        }
        $view_data = new stdClass();

        $regions = new City('', 0);
        $view_data->regions = $regions->getRegions();

        $recent_ads = new Ad(0);
        $view_data->recent_ads = $recent_ads->getAds(6);

        $this->viewWithData('App/Views/Home.php', $view_data);
    }


    public function cities(string $region): void
    {
        $cities = new City('byRegion', $region);
        $cities = $cities->getCitiesByRegion();
        $result = array();
        foreach ($cities as $city) {
            $result[] = $city['city'];
        }
        echo json_encode($result);
    }
}