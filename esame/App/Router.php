<?php

class Router
{
    public $url;

    public function __construct()
    {
        /*
         * php.net/manual/en/function.trim.php
         * Removes spaces, tabs, carriage return and NUL-byte
         */

        $this->url = trim($_SERVER['REQUEST_URI']);
    }

    public function callController(): void
    {
        $controller = $this->parse();
        $path = 'App/Controllers/' . $controller['name'] . '.php';

        /*
         * If Controller is not found, load default index
         */

        if (!file_exists($path)) {
            $path = 'App/Controllers/HomeController.php';
            $controller['name'] = 'HomeController';
            $controller['action'] = 'index';
            $controller['param'] = array(NULL);
        }

        require $path;
        $controllerInstance = new $controller['name']();
        call_user_func_array([$controllerInstance, $controller['action']], $controller['param']);
    }

    private function parse()
    {
        $url_segments = explode('/', $this->url);
        //var_dump($this->url);
        //var_dump($url_segments);

        switch ($url_segments[1]) {
            case 'registration':
                $controller['name'] = 'RegistrationController';
                break;
            case 'login':
                $controller['name'] = 'LoginController';
                break;
            case 'profile':
                $controller['name'] = 'ProfileController';
                break;
            case 'ad':
                $controller['name'] = 'AdController';
                break;
            case 'search':
                $controller['name'] = 'SearchController';
                break;
            case 'insert':
                $controller['name'] = 'InsertController';
                break;
            default:
                $controller['name'] = 'HomeController';
                break;
        }

        $controller['action'] = $url_segments[2] ?? 'index';

        if(isset($url_segments[3])) {
            $controller['param'] = array(urldecode($url_segments[3]));
        } else {
            $controller['param'] = array();
        }
        return $controller;
    }
}
