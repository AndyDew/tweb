<?php

class Ad extends Model
{
    public $id;
    public $user;
    public $title;
    public $body;
    public $price;
    public $published;
    public $active;
    public $city;
    public $img1;
    public $table = 'ads';

    public function __construct(int $id)
    {
        $this->id = $id;

        if ($id !== 0) {
            $ad = $this->fetchById();

            $this->user = $ad['user'];
            $this->title = $ad['title'];
            $this->body = $ad['body'];
            $this->price = $ad['price'];
            $this->published = $ad['published'];
            $this->active = $ad['active'];
            $this->city = $ad['city'];
            $this->img1 = $ad['img1'];
        }
    }

    public function create(): bool
    {
        $create = App::getDB()->prepare('INSERT INTO ' . $this->table . ' VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?)');
        $create->bindParam(1, $this->user);
        $create->bindParam(2, $this->title);
        $create->bindParam(3, $this->body);
        $create->bindParam(4, $this->price);
        $create->bindParam(5, $this->published);
        $create->bindParam(6, $this->active);
        $create->bindParam(7, $this->city);
        $create->bindParam(8, $this->img1);
        $create->execute();
        return $create->rowCount() === 0;
    }

    public function update(): bool
    {
        $create = App::getDB()->prepare('UPDATE ' . $this->table . ' SET user = ?, title = ?, body = ?, price = ?, published = ?,active = ?, city = ?, img1 = ?');
        $create->bindParam(1, $this->table);
        $create->bindParam(2, $this->user);
        $create->bindParam(3, $this->title);
        $create->bindParam(4, $this->body);
        $create->bindParam(5, $this->price);
        $create->bindParam(6, $this->published);
        $create->bindParam(7, $this->active);
        $create->bindParam(8, $this->city);
        $create->bindParam(9, $this->img1);
        $create->execute();
        return $create->rowCount() === 0;
    }

    public function getUser(): User
    {
        $user = App::getDB()->prepare('SELECT * FROM user WHERE id = ?');
        $user->bindParam(1, $this->user);
        $user->execute();
        return $user->fetch(PDO::FETCH_CLASS, 'User');
    }

    public function getCity(): City
    {
        $city = App::getDB()->prepare('SELECT * FROM cities WHERE id = ?');
        $city->bindParam(1, $this->city);
        $city->execute();
        return $city->fetch(PDO::FETCH_CLASS, 'City');
    }

    public function getAds(int $max): array
    {
        $ads = App::getDB()->prepare('SELECT * FROM ' . $this->table . ' LIMIT ?');
        $ads->bindParam(1, $max, PDO::PARAM_INT);
        $ads->execute();
        return $ads->fetchAll(PDO::FETCH_ASSOC);
    }

    public function searchAds(string $term, string $region, string $city): array
    {
        $region = urldecode($region);
        $ads = App::getDB()->prepare('SELECT * FROM ' . $this->table . ',cities WHERE cities.id = ' . $this->table . '.city AND cities.city = ? AND 
                                                cities.region = ? AND ' . $this->table . '.title LIKE ?');
        $ads->bindParam(1, $city);
        $ads->bindParam(2, $region);
        $search = '%' . $term . '%';
        $ads->bindParam(3, $search);
        $ads->execute();
        return $ads->fetchAll(PDO::FETCH_ASSOC);
    }
}
