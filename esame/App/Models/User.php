<?php

class User extends Model
{
    public $id;
    public $username;
    public $email;
    public $password;
    public $is_admin;
    public $new_password = null;
    public $table = 'users';

    public function __construct(int $id)
    {
        $this->id = $id;

        if ($id !== 0) {
            $user = $this->fetchById();

            $this->username = $user['username'];
            $this->email = $user['email'];
            $this->password = $user['password'];
            $this->is_admin = $user['is_admin'];
        }
    }

    public function fetchByUsername(): void
    {
        if ($this->id === 0) {
            $fetch = App::getDB()->prepare('SELECT * FROM ' . $this->table . ' WHERE username = ?');
            $fetch->bindParam(1, $this->username);
            $fetch->execute();
            $fetch = $fetch->fetch(PDO::FETCH_ASSOC);
            $this->id = $fetch['id'];
            $this->username = $fetch['username'];
            $this->email = $fetch['email'];
            $this->password = $fetch['password'];
            $this->is_admin = $fetch['is_admin'];
        }
    }

    public function create(): bool
    {
        if ($this->canRegister()) {
            $this->passwordHashing();
            $create = App::getDB()->prepare('INSERT INTO ' . $this->table . ' VALUES(NULL, ?, ?, ?, ?)');
            $create->bindParam(1, $this->username);
            $create->bindParam(2, $this->email);
            $create->bindParam(3, $this->password);
            $create->bindParam(4, $this->is_admin);
            $create->execute();
            return $create->rowCount() === 1;
        }
        return false;
    }

    private function canRegister(): bool
    {
        if ($this->id === 0 && isset($this->username, $this->email, $this->new_password)) {
            $rows = App::getDB()->prepare('SELECT * FROM ' . $this->table . ' WHERE username = ? OR email = ?');
            $rows->bindParam(1, $this->username);
            $rows->bindParam(2, $this->email);
            $rows->execute();
            return $rows->rowCount() === 0;
        }
        return false;
    }

    private function passwordHashing(): void
    {
        if ($this->new_password !== null) {
            $this->password = password_hash($this->new_password, PASSWORD_DEFAULT);
        }
    }

    public function update(): bool
    {
        $this->passwordHashing();
        $update = App::getDB()->prepare('UPDATE ' . $this->table . ' SET username = ?, email = ?, password = ?, is_admin = ?');
        $update->bindParam(1, $this->username);
        $update->bindParam(2, $this->email);
        $update->bindParam(3, $this->password);
        $update->bindParam(4, $this->is_admin);
        $update->execute();
        return $update->rowCount();
    }

    public function getUserAds(int $max, bool $onlyActive): array
    {
        $ads = App::getDB()->prepare('SELECT * FROM ads WHERE user = ? AND active = ? LIMIT ?');
        $ads->bindParam(1, $this->id);
        $ads->bindParam(2, $onlyActive);
        $ads->bindParam(3, $max);
        $ads->execute();
        return $ads->fetchAll(PDO::FETCH_CLASS, 'Ad');
    }
}