<?php

class City extends Model
{
    public $city;
    public $region;
    private $table = 'cities';
    private $pagination_offset = 0;

    public function __construct(string $mode, $value)
    {
        switch ($mode) {
            case 'byCity':
                $row = App::getDB()->prepare('SELECT * FROM ' . $this->table . ' WHERE city = ?');
                $row->bindParam(1, $this->table);
                $row->bindParam(2, $value);
                $row->execute();
                $this->populate($row->fetch(PDO::FETCH_ASSOC));
                break;
            case 'byRegion':
                $this->region = $value;
                break;
            default:
                $this->id = $value;
                if ($this->id !== 0) {
                    $this->populate($this->fetchById());
                }
                break;
        }
    }

    private function populate(array $row): void
    {
        $this->id = $row['id'];
        $this->city = $row['city'];
        $this->region = $row['region'];
    }

    public function getAds(int $max, bool $onlyActive): array
    {
        $ads = App::getDB()->prepare('SELECT * FROM ads WHERE city = ? AND active = ? LIMIT ? OFFSET  ?');
        $ads->bindParam(1, $this->id);
        $ads->bindParam(2, $onlyActive);
        $ads->bindParam(3, $max);
        $ads->execute();
        return $ads->fetchAll(PDO::FETCH_CLASS, 'Ad');
    }

    public function getRegions(): array
    {
        $regions = App::getDB()->query('SELECT DISTINCT region FROM ' . $this->table . ';');
        return $regions->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getCitiesByRegion(): array
    {
        $cities = App::getDB()->prepare('SELECT city FROM ' . $this->table . ' WHERE region = ?');
        $cities->bindParam(1, $this->region);
        $cities->execute();
        return $cities->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getCities(): array
    {
        $cities = App::getDB()->query('SELECT id,city FROM ' . $this->table . ';');
        return $cities->fetchAll(PDO::FETCH_ASSOC);
    }
}
