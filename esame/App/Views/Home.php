<?php
$page_title = 'Home - ' . App::$app_title;
require 'App/Views/common/Header.php';
echo '<body>';
require 'App/Views/common/Navigation.html';
?>
    <h1 class="text-center text-3xl mt-10"><span class="uppercase">Vendi ciò che non usi più,</span><br/>Inserisci i
        tuoi annunci in modo facile e veloce.</h1>
    <form class="sm:w-4/5 md:w-3/4 lg:w-1/2 mx-auto mt-16" action="/search" method="post" id="searchForm">
        <div class="flex items-center border-b border-b-2 border-teal-500 py-2">
            <input class="appearance-none bg-transparent border-none w-full text-gray-700 mr-3 py-1 px-2 leading-tight focus:outline-none"
                   type="text" placeholder="Cosa cerchi?" aria-label="Cosa Cerchi?" name="search_query" required>
            <select class="appearance-none bg-transparent border-none w-full text-gray-700 mr-3 py-1 px-2 leading-tight focus:outline-none"
                    id="regionSelector" name="regionSelector" required>
                <option disabled selected value="default">Regione</option>
                <?php
                foreach ($data->regions as $region) {
                    echo '<option value="' . $region['region'] . '">' . $region['region'] . '</option>';
                }
                ?>
            </select>
            <select class="appearance-none bg-transparent border-none w-full text-gray-700 mr-3 py-1 px-2 leading-tight focus:outline-none"
                    id="citySelector" name="citySelector"required>
                <option disabled selected value="default">Città</option>
            </select>
            <button class="flex-shrink-0 bg-teal-500 hover:bg-teal-700 border-teal-500 hover:border-teal-700 text-sm border-4 text-white py-1 px-2 rounded"
                    type="submit">
                Cerca
            </button>
        </div>
    </form>
    <h1 class="text-center text-3xl mt-16 mb-5 capitalize">Aggiunti di recente</h1>
    <div class="container mx-auto">
        <div class="flex flex-wrap -m-5">
            <?php
            foreach ($data->recent_ads as $ad) {
                echo '
                    <a class="sm:w-full md:w-1/3 rounded overflow-hidden shadow-lg inline-block flex flex-col p-5" href="/ad/show/'.$ad['id'].'">
                        <img class="w-full" src="/uploads/'.$ad['img1'].'">
                        <div class="px-6 py-4">
                            <div class="font-bold text-xl mb-2">'.htmlspecialchars($ad['title']).'</div>
                            <p class="text-gray-700 text-base">'.htmlspecialchars(substr($ad['body'],0,250)).'...</p>
                        </div>
                    </a>';
            }
            ?>
        </div>
    </div>
    <div class="container mx-auto mt-16 bg-gray-100">
        <h1 class="text-center text-2xl pt-6 text-gray-500">Scarica l’App ufficiale di Sell.it</h1>
        <h2 class="text-center text-gray-800">Cerca tra migliaia di annunci e inserisci i tuoi, ovunque tu sia.</h2>
        <div class="flex justify-center mt-6 pb-6">
            <img class="inline-block" src="/assets/img/play.svg">
            <img class="inline-block" src="/assets/img/appstore.svg">
        </div>
    </div>
    <div class="flex flex-shrink-0 bg-teal-500 p-6">
        <h1 class="text-white"><?php echo App::$copyright; ?></h1>
    </div>
<?php
require 'App/Views/common/Footer.php';
