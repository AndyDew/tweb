<?php
$page_title = 'Profilo - ' . App::$app_title;
require 'App/Views/common/Header.php';
echo '<body>';
require 'App/Views/common/Navigation.html';
?>
    <div class="container mx-auto p-16">
        <div class="w-full bg-gray-200 p-4">
            <h1 class="text-gray-900 uppercase text-center font-extrabold">Dati personali</h1>
        </div>
        <div class="w-full bg-gray-100 pt-10">
            <img class="rounded-full w-1/4 mx-auto mb-10" src="/assets/img/lock.png">
            <table class="table-auto mx-auto">
                <tr>
                    <td class="text-gray-500 font-bold">Email</td>
                    <td class="text-gray-700 pl-5"><?php echo $_SESSION['email'];?></td>
                </tr>
                <tr>
                    <td class="text-gray-500 font-bold">Username</td>
                    <td class="text-gray-700 pl-5"><?php echo $_SESSION['username'];?></td>
                </tr>
            </table>
            <div class="w-full bg-gray-200 p-4 mt-12">
                <h1 class="text-gray-900 uppercase text-center font-extrabold">Cambia password</h1>
            </div>
            <form class="w-full max-w-sm mx-auto pt-16" id="changePassword">
                <div class="md:flex md:items-center mb-6">
                    <div class="md:w-1/3">
                        <label class="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4"
                               for="password">
                            Password Attuale
                        </label>
                    </div>
                    <div class="md:w-2/3">
                        <input class="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                               id="password" name="password" type="password" placeholder="******************">
                    </div>
                </div>
                <div class="md:flex md:items-center mb-6">
                    <div class="md:w-1/3">
                        <label class="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4"
                               for="new_password">
                            Nuova Password
                        </label>
                    </div>
                    <div class="md:w-2/3">
                        <input class="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                               id="new_password" name="new_password" type="password" placeholder="******************">
                    </div>
                </div>
                <div class="md:flex md:items-center">
                    <div class="md:w-1/3"></div>
                    <div class="md:w-2/3">
                        <button class="shadow bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded"
                                type="submit">
                            Cambia Password
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="w-full bg-teal-500 p-6 absolute bottom-0">
        <h1 class="text-white"><?php echo App::$copyright; ?></h1>
    </div>
<?php
require 'App/Views/common/Footer.php';
