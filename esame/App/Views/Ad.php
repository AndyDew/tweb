<?php
$page_title = 'Annuncio - ' . App::$app_title;
require 'App/Views/common/Header.php';
echo '<body>';
require 'App/Views/common/Navigation.html';
?>
    <div class="container mx-auto p-16">
        <div class="w-full bg-gray-200 p-4">
            <h1 class="text-gray-900 uppercase text-center font-extrabold"><?php echo htmlspecialchars($data->ad->title);?></h1>
        </div>
        <div class="w-full bg-gray-100 pt-10">
            <div class="flex flex-wrap -m-5">
                <img class="sm:w-full md:w-2/3 md:inline-block flex flex-col p-5" src="/uploads/<?php echo $data->ad->img1 ;?>">
                <div class="sm:w-full md:w-1/3 md:inline-block flex flex-col p-5">
                    <h1 class="inline-block">Data: <?php echo date('Y-m-d', strtotime($data->ad->published));?></h1>
                    <h1 class="inline-block float-right">ID <?php echo htmlspecialchars($data->ad->id);?></h1>
                    <hr class="pt-2 pb-2">
                    <h1>Username <?php echo htmlspecialchars($data->owner->username);?></h1>
                    <hr class="pt-2 pb-2">
                    <h1 class="pt-2 pb-2 text-center font-bold">Descrizione</h1>
                    <p><?php echo htmlspecialchars($data->ad->body);?></p>
                </div>
            </div>
        </div>
        <?php
            if ($data->owner->id === $_SESSION['id'] || $_SESSION['is_admin']) {
                echo '<div class="w-full bg-gray-200 p-4 mt-10">
            <h1 class="text-gray-900 uppercase font-extrabold text-center">Hai già venduto questo articolo?</h1>
            <a class="w-1/3 text-center block mt-5 mx-auto shadow bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded"
            href="/ad/rm/'.$data->ad->id.'">Rimuovi Annuncio</a>
        </div>';
            }
        ?>
    </div>
    <div class="w-full bg-teal-500 p-6 absolute bottom-0">
        <h1 class="text-white"><?php echo App::$copyright; ?></h1>
    </div>
<?php
require 'App/Views/common/Footer.php';
