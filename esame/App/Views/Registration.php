<?php
$page_title = 'Registrazione - ' . App::$app_title;
require 'App/Views/common/Header.php';
?>
    <body>
<div class="flex h-screen">
    <div class="w-full max-w-xs m-auto">
        <form class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4" id="registrationForm">
            <img class="w-2/4 rounded-full m-auto" src="/assets/img/writing.png" alt="Lock Icon Rounded">
            <div class="mb-4 mt-6">
                <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                       id="username" name="username" type="text" placeholder="Username" required="required"
                       pattern="[A-Za-z0-9]{1,20}">
            </div>
            <div class="mb-4 mt-6">
                <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                       id="email" type="email" name="email" placeholder="Email" required="required">
            </div>
            <div class="mb-6">
                <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                       id="password" type="password" name="password" placeholder="Password" required="required"
                       pattern="[A-Za-z0-9]{1,20}">
            </div>
            <div class="flex items-center justify-between">
                <button class="mx-auto bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                        type="submit">
                    Registrati
                </button>
                <a class="mx-auto bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                   href="/login">
                    Login
                </a>
            </div>
        </form>
        <p class="text-center text-gray-500 text-xs">
            <?php echo App::$copyright; ?>
        </p>
    </div>
</div>
<?php
require 'App/Views/common/Footer.php';
