<?php
$page_title = 'Ricerca - ' . App::$app_title;
require 'App/Views/common/Header.php';
echo '<body>';
require 'App/Views/common/Navigation.html';
?>
    <div class="container mx-auto p-16">
        <div class="w-full bg-gray-200 p-4">
            <h1 class="text-gray-900 uppercase text-center font-extrabold"><?php echo $data->term;?></h1>
        </div>
        <div class="w-full bg-gray-100 pt-10">
            <?php
            foreach ($data->ads as $ad) {
                echo '<a href="/ad/show/'.$ad['id'].'">
                        <div class="max-w-sm w-full lg:max-w-full lg:flex mx-auto pb-6">
                            <div class="h-48 lg:h-auto lg:w-48 flex-none bg-cover rounded-t lg:rounded-t-none lg:rounded-l text-center overflow-hidden"
                                 style="background-image: url(\'/uploads/'.$ad['img1'].'\')"
                                 title="Woman holding a mug">
                            </div>
                            <div class="border-r border-b border-l border-gray-400 lg:border-l-0 lg:border-t lg:border-gray-400 bg-white rounded-b lg:rounded-b-none lg:rounded-r p-4 flex flex-col justify-between leading-normal">
                                <div class="mb-8">
                                    <div class="text-gray-900 font-bold text-xl mb-2">'.htmlspecialchars($ad['title']).'</div>
                                    <p class="text-gray-700 text-base">'.htmlspecialchars($ad['body']).'</p>
                                </div>
                                <div class="flex items-center">
                                    <div class="text-sm">
                                        <p class="text-gray-900 leading-none font-extrabold">'.$ad['city'].', '.$ad['region'].'</p>
                                        <p class="text-gray-600">'.date('Y-m-d', strtotime($ad['published'])).'</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>';
            }
            ?>

        </div>
    </div>
    <div class="w-full bg-teal-500 p-6 absolute bottom-0">
        <h1 class="text-white"><?php echo App::$copyright; ?></h1>
    </div>
<?php
require 'App/Views/common/Footer.php';
