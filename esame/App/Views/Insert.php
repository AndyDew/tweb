<?php
$page_title = 'Annuncio - ' . App::$app_title;
require 'App/Views/common/Header.php';
echo '<body>';
require 'App/Views/common/Navigation.html';
?>
    <div class="container mx-auto p-16">
        <div class="w-full bg-gray-200 p-4">
            <h1 class="text-gray-900 uppercase text-center font-extrabold">Inserisci il tuo annuncio</h1>
        </div>
        <?php
        if (isset($data->uploaded)) {
            if ($data->uploaded) {
                $msg = 'Il tuo annuncio è stato pubblicato';
            } else {
                $msg = 'Qualcosa è andato storto, riprova';
            }
            echo '<div class="bg-indigo-900 text-center py-4 lg:px-4">
                    <div class="p-2 bg-indigo-800 items-center text-indigo-100 leading-none lg:rounded-full flex lg:inline-flex" role="alert">
                        <span class="flex rounded-full bg-indigo-500 uppercase px-2 py-1 text-xs font-bold mr-3">Notifica</span>
                        <span class="font-semibold mr-2 text-left flex-auto">'.$msg.'</span>
                    </div>
                 </div>';
        }
        ?>
        <div class="w-full bg-gray-100 pt-10">
            <form class="w-3/4 mx-auto pb-10" id="insertForm" action="/insert/add" method="POST" enctype="multipart/form-data">
                <div class="flex items-center border-b border-b-2 border-teal-500 py-2" id="insert-1">
                    <input class="h-16 text-3xl appearance-none bg-transparent border-none w-full text-gray-700 mr-3 py-1 px-2 leading-tight focus:outline-none"
                           type="text" placeholder="Inserisci un titolo" aria-label="Inserisci un titolo" name="title" required>
                    <button class="flex-shrink-0 bg-teal-500 hover:bg-teal-700 border-teal-500 hover:border-teal-700 text-sm border-4 text-white py-1 px-2 rounded"
                            type="button" id="btn-i-1">
                        Continua
                    </button>
                </div>
                <div class="flex items-center border-b border-b-2 border-teal-500 py-2 hidden" id="insert-2">
                    <input class="h-16 text-3xl appearance-none bg-transparent border-none w-full text-gray-700 mr-3 py-1 px-2 leading-tight focus:outline-none"
                           type="number" placeholder="Inserisci il prezzo" aria-label="Inserisci un prezzo" name="price" required>
                    <button class="flex-shrink-0 bg-teal-500 hover:bg-teal-700 border-teal-500 hover:border-teal-700 text-sm border-4 text-white py-1 px-2 rounded"
                            type="button" id="btn-i-2">
                        Continua
                    </button>
                </div>
                <div class="flex items-center border-b border-b-2 border-teal-500 py-2 hidden" id="insert-4">
                    <select class="h-16 text-3xl appearance-none bg-transparent border-none w-full text-gray-700 mr-3 py-1 px-2 leading-tight focus:outline-none"
                            id="grid-state" name="city">
                        <option selected disabled value="default">Città</option>
                        <?php
                            foreach ($data->cities as $city) {
                                echo '<option value="'.$city['id'].'">'.$city['city'].'</option>';
                            }
                        ?>
                    </select>
                    <button class="flex-shrink-0 bg-teal-500 hover:bg-teal-700 border-teal-500 hover:border-teal-700 text-sm border-4 text-white py-1 px-2 rounded"
                            type="button" id="btn-i-4">
                        Continua
                    </button>
                </div>
                <div class="flex items-center border-b border-b-2 border-teal-500 py-2 hidden" id="insert-5">
                    <textarea placeholder="Inserisci una descrizione" name="body" required
                            class="appearance-none bg-transparent border-none w-full text-gray-700 mr-3 py-1 px-2 leading-tight focus:outline-none"></textarea>
                    <button class="flex-shrink-0 bg-teal-500 hover:bg-teal-700 border-teal-500 hover:border-teal-700 text-sm border-4 text-white py-1 px-2 rounded"
                            type="button" id="btn-i-5">
                        Continua
                    </button>
                </div>
                <div class="border-b border-b-2 border-teal-500 py-2 hidden" id="insert-6">
                    <h1 class="text-center font-bold">Carica delle immagini</h1>
                    <input class="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4" type="file" id="btn-i-6" name="picture" required>
                    <div class="flex justify-center pt-5">
                        <button class="flex-shrink-0 bg-teal-500 hover:bg-teal-700 border-teal-500 hover:border-teal-700 text-sm border-4 text-white py-1 px-2 rounded"
                                type="submit">
                            Continua
                        </button>
                        <button class="flex-shrink-0 border-transparent border-4 text-teal-500 hover:text-teal-800 text-sm py-1 px-2 rounded"
                                type="reset">
                            Reset
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="w-full bg-teal-500 p-6 absolute bottom-0">
        <h1 class="text-white"><?php echo App::$copyright; ?></h1>
    </div>
<?php
require 'App/Views/common/Footer.php';
