<?php
session_start();
require 'App/App.php';
require 'App/Router.php';
require 'App/Model.php';
require 'App/Response.php';

require 'App/Models/Ad.php';
require 'App/Models/City.php';
require 'App/Models/User.php';

require 'App/Controller.php';

$route = new Router();
$route->callController();