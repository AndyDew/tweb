$('#registrationForm').submit(function (event) {
    event.preventDefault();

    let formData = formToJson($('#registrationForm'));
    //console.log(formData);

    if (typeof formData.username !== 'undefined' && typeof formData.email !== 'undefined' && typeof formData.password !== 'undefined') {
        let request = $.ajax({
            type: "POST",
            url: '/registration/api',
            data: JSON.stringify(formData),
            dataType: 'json'
        });
        request.fail(function () {
            alert('Connessione al server fallita!');
        });
        request.done(function (response) {
            alert(response.data.msg);
            if (response.status === 'OK') {
                window.location.href = '/login';
            }
        })
    }
});

$('#loginForm').submit(function (event) {
    event.preventDefault();

    let formData = formToJson($('#loginForm'));
    console.log(formData);

    if (typeof formData.username !== 'undefined' && typeof formData.password !== 'undefined') {
        let request = $.ajax({
            type: "POST",
            url: '/login/api',
            data: JSON.stringify(formData),
            dataType: 'json'
        });
        request.fail(function (r) {
            console.log(r);
            alert('Connessione al server fallita!');
        });
        request.done(function (response) {
            //console.log(response);
            alert(response.data.msg);
            if (response.status === 'OK') {
                window.location.href = '/';
            }
        })
    }
});

$('#regionSelector').change(function () {
    let cities = [];
    let select_value = $('#regionSelector option:selected').text();
    $.get('/home/cities/' + encodeURI(select_value), function (data) {
        $('#citySelector option:enabled').remove();
        $.each(JSON.parse(data), function (index, value) {
            $('#citySelector').append('<option value="' + value + '">' + value + '</option>');
        });
    });
});

$('#searchForm').submit(function (event) {
    let formData = formToJson($('#searchForm'));
    console.log(formData);

    if (typeof formData.search_query !== 'undefined' &&
        typeof formData.regionSelector !== 'undefined' &&
        typeof formData.citySelector !== 'undefined' &&
        formData.citySelector !== 0 &&
        formData.regionSelector !== 'default') {
    } else {
        event.preventDefault();
        alert('Verifica di aver inserito correttamente i dati!');

    }
});

$('#changePassword').submit(function (event) {
    event.preventDefault();
    let formData = formToJson($('#changePassword'));
    //console.log(formData);

    if (typeof formData.password !== 'undefined' && typeof formData.new_password !== 'undefined') {
        let request = $.ajax({
            type: "POST",
            url: '/profile/passwordChange',
            data: JSON.stringify(formData),
            dataType: 'json'
        });
        request.fail(function (r) {
            alert('Connessione al server fallita!');
        });
        request.done(function (response) {
            //console.log(response);
            alert(response.data.msg);
            if (response.status === 'OK') {
                window.location.href = '/login/out';
            }
        })
    }

});

$('#insertForm').submit(function (event) {
    let formData = formToJson($('#insertForm'));
    //console.log(formData);

    if (typeof formData.body === 'undefined' || formData.city === 'default' ||
        typeof formData.price === 'undefined' || typeof formData.title === 'undefined') {
        event.preventDefault();
        alert('Verifica di aver inserito correttamente i dati!');
    }
});

$('#btn-i-1').click(function (event) {
    $('#insert-1').addClass('hidden');
    $('#insert-2').removeClass('hidden');
});

$('#btn-i-2').click(function (event) {
    $('#insert-2').addClass('hidden');
    $('#insert-4').removeClass('hidden');
});

$('#btn-i-4').click(function (event) {
    $('#insert-4').addClass('hidden');
    $('#insert-5').removeClass('hidden');
});

$('#btn-i-5').click(function (event) {
    $('#insert-5').addClass('hidden');
    $('#insert-6').removeClass('hidden');
});

function formToJson($form) {
    let unindexed_array = $form.serializeArray();
    let indexed_array = {};

    $.map(unindexed_array, function (n, i) {
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}