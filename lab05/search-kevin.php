<?php
include_once 'top.html';
include_once 'common.php';

echo "<h1>Results for {$_GET['firstname']} {$_GET['lastname']}</h1>";

if ($actor==null) {
    echo "<p>Actor {$_GET['firstname']} {$_GET['lastname']} not found!</p>";
} else {
    $movies = $pdo->prepare("SELECT movies.name,movies.year 
                                          FROM movies,roles  
                                          WHERE movies.id=roles.movie_id
                                            AND roles.actor_id = ?
                                            AND movies.id IN 
                                              (SELECT DISTINCT movies.id 
                                                FROM roles 
                                                  JOIN movies ON movies.id=roles.movie_id 
                                                  JOIN actors ON actors.id=roles.actor_id 
                                                WHERE actors.first_name='Kevin' AND actors.last_name='Bacon')");
    $movies->bindParam(1, $actor);
    $movies->execute();
    $movies->execute();
    $movie_count = $movies->rowCount();

    if($movie_count===0) {
        echo "<p>{$_GET['firstname']} {$_GET['lastname']} wasn't in any films with Kevin Bacon!</p>";
    } else {
        $movies = $movies->fetchAll();
        echo '<p>Films '.$_GET['firstname'].' '.$_GET['lastname'].' with and Kevin Bacon</p>
             <table>
                <thead>
                    <th>#</th>
                    <th>Title</th>
                    <th>Year</th>
                </thead>
                <tbody>';

        for($i = 0; $i < $movie_count; $i++) {
            if($i%2===0) {
                $tr_class = 'even';
            } else {
                $tr_class = 'odd';
            }

            echo '<tr class="'.$tr_class.'">
                    <td class="td_size">'.($i+1).'</td>
                    <td>'.$movies[$i]['name'].'</td>
                    <td class="td_size">'.$movies[$i]['year'].'</td>
                  </tr>';
        }
        echo    '</tbody>
             </table>';
    }
}
include_once 'bottom.html';