<?php
include_once 'top.html';
include_once 'common.php';

echo "<h1>Results for {$_GET['firstname']} {$_GET['lastname']}</h1>";

if ($actor===null) {
    echo "<p>Actor {$_GET['firstname']} {$_GET['lastname']} not found!</p>";
} else {
    $movies = $pdo->prepare("SELECT movies.name, movies.year 
                                          FROM movies,roles
                                          WHERE roles.movie_id=movies.id
                                          AND roles.actor_id=?
                                          ORDER BY movies.year DESC");
    $movies->bindParam(1,$actor);
    $movies->execute();
    $movie_count = $movies->rowCount();
    if($movie_count===0) {
        echo "<p>Actor {$_GET['firstname']} {$_GET['lastname']} has no films!</p>";
    } else {
        $movies = $movies->fetchAll();
        echo '<p>All films</p>
             <table>
                <thead>
                    <th>#</th>
                    <th>Title</th>
                    <th>Year</th>
                </thead>
                <tbody>';
        for($i = 0; $i < $movie_count; $i++) {
            if($i%2===0) {
                $tr_class = 'even';
            } else {
                $tr_class = 'odd';
            }

            echo '<tr class="'.$tr_class.'">
                    <td class="td_size">'.($i+1).'</td>
                    <td>'.$movies[$i]['name'].'</td>
                    <td class="td_size">'.$movies[$i]['year'].'</td>
                  </tr>';
        }
        echo    '</tbody>
             </table>';
    }

}
include_once 'bottom.html';