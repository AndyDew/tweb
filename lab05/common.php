<?php
$pdo = new PDO("mysql:dbname=lab05;host=localhost", "root", "");

$actor = $pdo->prepare("SELECT actors.id, COUNT(*) as c 
                                    FROM actors
                                    WHERE actors.first_name LIKE ? 
                                      AND actors.last_name LIKE ? 
                                    GROUP BY actors.id 
                                    ORDER BY c DESC ,actors.id ASC
                                    LIMIT 1");
$firstname = $_GET['firstname'].'%';
$lastname = $_GET['lastname'].'%';
$actor->bindParam(1,$firstname);
$actor->bindParam(2,$lastname);
$actor->execute();
if ($actor->rowCount()==1) {
    $actor = $actor->fetchAll()[0]['id'];
} else {
    $actor = null;
}