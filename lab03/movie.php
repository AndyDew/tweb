<?php
$movie=$_GET['film'];

/*
 * Reading from txt files
 * explode string (whole file content) results in array of strings
 * each line is available as array index [N-1]
 */
$info = explode(PHP_EOL,file_get_contents($movie.'/info.txt'));
$overview = explode(PHP_EOL,file_get_contents($movie.'/overview.txt'));

/*
 * Using glob() to parse all file names matching "reviews" pattern
 * The for loop picks up the first 10 reviews
 * --- inverting the loop would pick uo the most recent reviews
 * explode string (whole file content) results in array of strings
 * checking file_gets_contents return is case available reviews are less than 10
 * using array_push() to store all the reviews in a multidimensional array $reviews
 */
$reviews_path = glob($movie.'/review*.txt');
$reviews = [];
for ($i=0;$i<10;$i++) {
    $review = file_get_contents($reviews_path[$i]);
    if($review!==false) {
        $review = explode(PHP_EOL,$review);
        array_push($reviews,$review);
    }
}

/*
 * Function expects an int var as $rating
 * returns the correct asset based on score
 */
function rotten($rating) {
    if($rating>=60) {
        return 'http://courses.cs.washington.edu/courses/cse190m/11sp/homework/2/freshbig.png';
    } else {
        return 'http://courses.cs.washington.edu/courses/cse190m/11sp/homework/2/rottenbig.png';
    }
}

/*
 * Function expects a string var as $rating
 * returns the correct asset based on reviewer's rating
 */
function is_fresh($rating) {
    if($rating==='ROTTEN') {
        return 'http://courses.cs.washington.edu/courses/cse190m/11sp/homework/2/rotten.gif';
    } else {
        return 'http://courses.cs.washington.edu/courses/cse190m/11sp/homework/2/fresh.gif';
    }
}

/*
 * Function reads previously imported content
 * (line #10 reference)
 * further explode is needed on ":" to separate terms from definitions
 * render content
 */
function render_overview($overview) {
    foreach ($overview as $line) {
        $line = explode(':',$line);
        echo '<dt>'.$line[0].'</dt>';
        echo '<dd>'.$line[1].'</dd>';
    }
}
?>



<!DOCTYPE html>
<html><!--aggiunta apertura del tag mancante-->
<head>
    <title>Rancid Tomatoes</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="icon" type="image/gif"
          href="http://courses.cs.washington.edu/courses/cse190m/11sp/homework/2/critic.gif"/>
    <link href="movie.css" type="text/css" rel="stylesheet">
</head>

<body>
<div id="banner">
    <img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/banner.png" alt="Rancid Tomatoes"
         class="center">
</div>

<h1><?= $info[0].' ('.$info[1].')'; //printing data from info.txt -- Title and release year ?></h1>

<div id="content" class="center">
    <div id="right">
        <div>
            <img src="<?= $movie.'/overview.png'; //printing film poster?>"
                 alt="general overview">
        </div>
        <dl>
            <?php render_overview($overview); //reference at line #60?>
        </dl>
    </div>
    <div id="left">
        <div id="rotten">
            <img src="<?= rotten($info[2]); //reference at line #34 ?>" alt="Rotten">
            <span><?= $info[2]; ?>%</span>
        </div>

        <div id="rev-left">
            <?php
            /*
             * Count the available reviews and render half of them
             * case odd, use round function for int results and exercise requirements
             */
            for ($i=0;$i<round(count($reviews)/2);$i++) {
                echo '<div class="quote-box">
                        <p class="quote-box-content">
                        <img src="'.is_fresh($reviews[$i][1]).'" alt="Rotten">
                        <q>'.$reviews[$i][0].'</q>
                    </div>
                    <p class="author">
                        <img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/critic.gif" alt="Critic">
                        '.$reviews[$i][2].'<br>                
                        <span class="i">'.$reviews[$i][3].' </span>
                    </p>';
            }
            ?>
        </div><!--
        Display:inline-grid hack; removed whitespacing and margin
        --><div id="rev-right">
            <?php
            /*
            * Count the available reviews and render half of them
            * pick from we left above
            */
            for ($i=round(count($reviews)/2);$i<count($reviews);$i++) {
                echo '<div class="quote-box">
                        <p class="quote-box-content">
                        <img src="'.is_fresh($reviews[(int)$i][1]).'" alt="Rotten">
                        <q>'.$reviews[(int)$i][0].'</q>
                    </div>
                    <p class="author">
                        <img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/critic.gif" alt="Critic">
                        '.$reviews[(int)$i][2].'<br>                
                        <span class="i">'.$reviews[(int)$i][3].' </span>
                    </p>';
            }
            ?>
        </div>
    </div>
    <div id="footer">
        <p><?= '(1-'.count($reviews).') of '.count($reviews);?></p>
    </div>
</div>
<div id="validation">
    <a href="ttp://validator.w3.org/check/referer"><img
            src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/w3c-xhtml.png"
            alt="Validate HTML"></a> <br>
    <a href="http://jigsaw.w3.org/css-validator/check/referer">
        <img src="http://jigsaw.w3.org/css-validator/images/vcss" alt="Valid CSS!">
    </a>
</div>
</body>
</html>
