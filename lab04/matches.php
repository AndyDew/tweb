<?php require_once 'top.html'; ?>
<form action="matches-submit.php" method="GET">
    <fieldset>
        <legend>Returning User:</legend>
        <label for="name" class="left">Name:</label>
        <input type="text" name="name" id="name" maxlength="16" pattern="[A-Za-z ]+">
        <br>
        <input type="submit" value="View My Matches">
    </fieldset>
</form>
<?php require_once 'bottom.html';
