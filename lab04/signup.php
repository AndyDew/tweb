<?php require_once 'top.html'; ?>
    <form action="signup-submit.php" method="POST">
        <fieldset>
            <legend>New User Signup:</legend>
            <label for="name" class="left"><strong>Name:</strong></label>
            <input type="text" name="name" id="name" max="16" pattern="[A-Za-\ ]+" required>
            <br>
            <label for="gender" class="left"><strong>Genders:</strong></label>
            <input type="radio" name="gender" id="gender" value="M">Male
            <input type="radio" name="gender" value="F" checked>Female
            <br>
            <label for="age" class="left"><strong>Age:</strong></label>
            <input type="text" name="age" id="age" size="6" maxlength="2" pattern="[0-9]+" required>
            <br>
            <label for="personality" class="left"><strong>Personality type:</strong></label>
            <input type="text" name="personality" id="personality" size="6" maxlength="4" required> (<a href="#">Don't
                know your type?</a>)
            <br>
            <label for="os" class="left"><strong>Favourite OS:</strong></label>
            <select name="os" id="os">
                <option value="Windows">Windows</option>
                <option value="Mac OS X">Mac OS X</option>
                <option value="Linux" selected>Linux</option>
            </select>
            <br>
            <label for="seeking" class="left"><strong>Seeking age:</strong></label>
            <input type="text" name="seeking_min" size="6" maxlength="2" pattern="[0-9]+" placeholder="min">
            <input type="text" name="seeking_max" size="6" maxlength="2" pattern="[0-9]+" placeholder="max">
            <br>
            <input type="submit" value="Sign Up">
        </fieldset>
    </form>
<?php require_once 'bottom.html';